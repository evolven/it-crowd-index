# IT Crowd Index

### About Evolven

Changes that have been introduced into the environment are most likely the cause of performance and availability incidents and Evolven is the only vendor that collects changes at the most granule level, treating them as the true root causes. Since there are disproportionally more changes than incidents, the main challenge is to highlight a needle in a haystack, that is, to pinpoint the changes that carry the highest risk of incident.

Evolven provides analytics technology that combines multiple data sources, thus creating a cross-silo perspective. The core component is Blended Analytics Engine leveraging machine learning, anomaly detection, causal correlation, and expert knowledge to track, correlate and analyze all data sources, end-to-end from application to infrastructure at the most granular level, to recognize high risk changes early on and remediate them even before they result in an incident. This technology introduces a number of unique capabilities required to investigate incidents, prevent incidents, and maintain the highest quality of service across IT environments.

### The problem

An input to Evolven Blended Analytics Engine is knowledge base specifying how important a specific parameter in the system is. The knowledge base is built manually by experts assigning impact of particular configuration to the system.

This task is time consuming, error prone, and requires continious effort to maintain the knolwedge base up to date with new technologies. Evolven hence wants to investigate an automatic way of assigning impact of configurations.


### Your task

Your taks is the check the hytpotesis that a hint how important a specific parameter is can be attained by looking into how frequently such parameter has been searched for. The task is hence to make a query to a search engine for a specific parameter and check the number of hits. According to hypotesis, high-impact parameters (causing issues) will tend to have more hits compared to parameters with low impact to the system.

The task is to write a [jupyter notebook](http://jupyter.org/) using [python 3](https://www.python.org), [pandas](http://pandas.pydata.org/) and [scikit-learn](http://scikit-learn.org/stable/) that will:
 - Load MS SQL configurations parameters from `data/Export_Environment_MSSQLSERVER.xml` file, look for *Key* in *System configuration node*
 - For each parameter, collect the number of hits on [Database Administrators StackExchange](http://dba.stackexchange.com) 
 - Load the actual configuration impact written in `data/table.json` file (look for `"Critical":"true"` property)
 - Identify the optimal threshold for the number of search hits to distinguish between critical/non critical parameters
 - Calculate precision, recall, and accuracy
 - Email the jupyter report to bostjan@evolven.com
 
 
 
The task is estimated to take 40-90 minutes (depends on your knowledge).
